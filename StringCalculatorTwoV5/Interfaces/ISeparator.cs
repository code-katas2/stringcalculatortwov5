﻿namespace StringCalculatorTwoV5
{
    public interface ISeparator
    {
        string[] GetSeparators(string numbers);
    }
}