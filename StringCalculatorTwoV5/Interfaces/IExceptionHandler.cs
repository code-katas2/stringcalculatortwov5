﻿using System.Collections.Generic;

namespace StringCalculatorTwoV5
{
    public interface IExceptionHandler
    {
        void ValidateNumbersGreaterThousand(List<int> numbersList);
    }
}