﻿using System.Collections.Generic;

namespace StringCalculatorTwoV5
{
    public class StringCalculator :IStringCalculator
    {
        private ISeparator _separator;
        private IDelimiter _delimiter;
        private IExceptionHandler _handler;

        public StringCalculator(ISeparator separator, IDelimiter delimiter, IExceptionHandler handler)
        {
            _separator = separator;
            _delimiter = delimiter;
            _handler = handler;
        }

        public int Subtract(string numbers)
        {
            var difference = 0;

            if (string.IsNullOrEmpty(numbers))
            {
                return difference;
            }

            var separatorArray = _separator.GetSeparators(numbers);
            var delimiterArray = _delimiter.GetDelimiters(numbers, separatorArray);
            var numbersList = _delimiter.GetNumbers(numbers, delimiterArray);

            _handler.ValidateNumbersGreaterThousand(numbersList);

            difference = GetDifference(numbersList);

            return difference;
        }

        private int GetDifference(List<int> numbersList)
        {
            var difference = 0;

            foreach (var number in numbersList)
            {
                if (number > 0)
                {
                    difference += number * -1;
                }
                else
                {
                    difference += number;
                }
            }

            return difference;
        }
    }
}
