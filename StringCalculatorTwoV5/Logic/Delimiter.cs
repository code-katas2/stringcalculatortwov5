﻿using System;
using System.Collections.Generic;

namespace StringCalculatorTwoV5
{
    public class Delimiter : IDelimiter
    {
        private ICommonCharacters _characters;

        public Delimiter(ICommonCharacters characters)
        {
            _characters = characters;
        }

        public string[] GetDelimiters(string numbers, string[] separatorArray)
        {
            var delimiterArray = new string[] { _characters.Comma.ToString(), _characters.NewLine.ToString() };

            if (numbers.StartsWith(_characters.Hash.ToString()))
            {
                var delimiter = string.Concat(numbers.Split(_characters.Hash));

                delimiter = delimiter.Substring(0, delimiter.IndexOf(_characters.NewLine));
                delimiterArray = new string[] { delimiter };
            }

            if (numbers.Contains(_characters.LeftSquare.ToString()) || numbers.Contains(_characters.LessThan.ToString()))
            {
                var hashPostion = numbers.IndexOf(_characters.Hash) + 2;
                var delimiter = numbers.Substring(hashPostion, numbers.IndexOf(_characters.NewLine) - hashPostion);

                delimiterArray = delimiter.Split(separatorArray, StringSplitOptions.RemoveEmptyEntries);
            }

            return delimiterArray;
        }

        public List<int> GetNumbers(string numbers, string[] delimiterArray)
        {
            var numbersList = new List<int>();

            if (numbers.Contains(_characters.Hash.ToString()))
            {
                numbers = numbers.Split(_characters.NewLine)[1];
            }

            foreach (var number in numbers.Split(delimiterArray, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumbers = int.Parse(number);

                    numbersList.Add(convertedNumbers);
                }
                catch
                {
                    var convertedLetter = (int)number.ToUpper()[0];

                    convertedLetter -= 65;

                    if (convertedLetter <= 9)
                    {
                        numbersList.Add(convertedLetter);
                    }
                }
            }

            return numbersList;
        }
    }
}
