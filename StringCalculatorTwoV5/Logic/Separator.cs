﻿
namespace StringCalculatorTwoV5
{
    public class Separator : ISeparator
    {
        private ICommonCharacters _characters;

        public Separator(ICommonCharacters characters)
        {
            _characters = characters;
        }

        public string[] GetSeparators(string numbers)
        {
            var separatorArray = new string[] { _characters.LeftSquare.ToString(), _characters.RightSquare.ToString() };

            if (numbers.StartsWith(_characters.LessThan.ToString()))
            {
                var delimiter = numbers.Substring(0, numbers.IndexOf(_characters.Hash));

                delimiter = delimiter.Remove(0,1);
                separatorArray = delimiter.Split( _characters.GreaterThan);
            }

            if (numbers.Contains(_characters.DoubleLessThan) || numbers.Contains(_characters.DoubleGreaterThan))
            {
                separatorArray = new string[] { _characters.LessThan.ToString(), _characters.GreaterThan.ToString() };
            }

            return separatorArray;
        }
    }
}
