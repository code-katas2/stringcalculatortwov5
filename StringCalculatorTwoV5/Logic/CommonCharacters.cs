﻿namespace StringCalculatorTwoV5
{
    public class CommonCharacters : ICommonCharacters
    {
        public char Comma { get; } = ',';
        public char NewLine { get; } = '\n';
        public char Hash { get; } = '#';
        public char LeftSquare { get; } = '[';
        public char RightSquare { get; } = ']';
        public char LessThan { get; } = '<';
        public char GreaterThan { get; } = '>';
        public string DoubleLessThan { get; } = "<<";
        public string DoubleGreaterThan { get; } = ">>";
    }
}
