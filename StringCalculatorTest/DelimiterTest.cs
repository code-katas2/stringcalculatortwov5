﻿using System;
using StringCalculatorTwoV5;
using NUnit.Framework;
using System.Collections.Generic;

namespace StringCalculatorTest
{
    [TestFixture]
    public class DelimiterTest
    {
        private IDelimiter _delimiter;

        [SetUp]
        public void SetUp()
        {
            ICommonCharacters characters = new CommonCharacters();
           
            _delimiter = new Delimiter(characters);
        }

        [Test]
        public void GivenStringWithSeparator_WhenGettingDelimiters_ThenReturnDelimiters()
        {
            //Arrange
            var separatorArray = new string[] { "[","]"};
            var expected = new string[] { "*" };

            //Act
            var actual = _delimiter.GetDelimiters("##[*]\n1*2*3",separatorArray);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenStringWithSeparators_WhenGettingMultipleDelimiters_ThenReturnDelimiters()
        {
            //Arrange
            var separatorArray = new string[] { "[","]"};
            var expected = new string[] { "*", "%" };

            //Act
            var actual = _delimiter.GetDelimiters("##[*][%]\n1*2%3",separatorArray);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenStringWithSeparators_WhenGettingAnyLengthDelimiters_ThenReturnDelimiters()
        {
            //Arrange
            var separatorArray = new string[] { "[","]"};
            var expected = new string[] { "*#$", "%$$" };

            //Act
            var actual = _delimiter.GetDelimiters("##[*#$][%$$]\n1*#$2%$$3", separatorArray);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenOneDelimiter_WhenGettingNumbers_ThenReturnNumbers()
        {
            //Arrange
            var delimiterArray = new string[] { "*"};
            var expected = new List<int> (new int[] { 1, 2,3 });

            //Act
            var actual = _delimiter.GetNumbers("##[*]\n1*2*3", delimiterArray);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        
    }
}
