﻿using System;
using StringCalculatorTwoV5;
using NUnit.Framework;

namespace StringCalculatorTest
{
    [TestFixture]
    public class SeparatorTest
    {
        private ISeparator _separator;

        [SetUp]
        public void SetUp()
        {
            ICommonCharacters characters = new CommonCharacters();

             _separator = new Separator(characters);
        }

        [Test]
        public void GivenNoFlagsString_WhenSeparatingDelimiters_ThenReturnSeparators()
        {
            //Arrange
            var expected = new string[]{"[","]" };

            //Act
            var actual = _separator.GetSeparators("##[*]\n1*2*3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenAStringWithFlags_WhenSeparatingDelimiters_ThenReturnSeparators()
        {
            //Arrange
            var expected = new string[]{"(",")" };

            //Act
            var actual = _separator.GetSeparators("<(>)##(*)\n1*2*3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenAStringWithFlagsAsDelimiters_WhenSeparatingDelimiters_ThenReturnSeparators()
        {
            //Arrange
            var expected = new string[]{"<",">" };

            //Act
            var actual = _separator.GetSeparators("<>><##>&<\n4&5&6");

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
